resource "hcloud_ssh_key" "sshkey01" {
  name       = "${var.prefix}-key-01"
  public_key = file("./id_rsa.pub")
}

resource "hcloud_network" "network01" {
  name     = "${var.prefix}-network-01"
  ip_range = "${var.ip_range}"
}

resource "hcloud_network_subnet" "subnet01" {
  network_id   = hcloud_network.network01.id
  type         = "cloud"
  network_zone = "eu-central"
  ip_range     = "${var.subnet_ip_range}"
}

resource "hcloud_firewall" "fw01" {
  name = "${var.prefix}-firewall-01"
  rule {
    direction       = "in"
    protocol        = "tcp"
    port            = 22
    source_ips      = ["0.0.0.0/0"]
  }
  rule {
    direction       = "out"
    protocol        = "tcp"
    port            = "any"
    destination_ips = ["0.0.0.0/0"]
  }
  rule {
    direction       = "out"
    protocol        = "udp"
    port            = "123"
    destination_ips = ["0.0.0.0/0"]
  }
}

resource "hcloud_volume" "vol01" {
  name      = "${var.prefix}-vol-01"
  size      = "${var.disk_size}"
  server_id = hcloud_server.vm01.id
  automount = true
  format    = "xfs"
}

resource "hcloud_server" "vm01" {
  name         = "${var.prefix}-vm-01"
  server_type  = "${var.server_type}"
  image        = "${var.os_type}"
  location     = "${var.location}"
  ssh_keys     = [hcloud_ssh_key.sshkey01.name]
  firewall_ids = [hcloud_firewall.fw01.id]
  user_data    = file("user-data.yml")
  public_net {
    ipv4_enabled = true
    ipv6_enabled = false
  }
  network {
    network_id = hcloud_network.network01.id
  }
  depends_on = [
    hcloud_network_subnet.subnet01
  ]
}
