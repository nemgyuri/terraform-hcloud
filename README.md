Terraform example for Hetzner Cloud
=======================

This example project demonstrates the automatic deployment of a Hetzner Cloud VM (size cx11) with terraform and cloud-init.

Prerequisites
--------------
- Terraform and Git installed
- A registered and (if necessary) verified Hetzner Cloud account 

How to use it
-------------
- Clone the repository via `git clone` or the GitLab web frontend.
- Create a new project in your Hetzner Cloud console and create an API token with read/write permission (important) for that project, [see instructions here](https://docs.hetzner.cloud/#overview-getting-started)
- Create a file named `secrets.auto.tfvars` in this directory with following content: `hcloud_token = "YOUR_GENERATED_API_TOKEN"`. Replace YOUR_GENERATED_API_TOKEN with the token generated in the previous step. A template for that file, named `secrets.auto.tfvars.example` is available in this repository as well.
- Swap out the SSH public key with your one, or you won't be able to login to the machine later. You need to replace the file `id_rsa.pub` with your public key, as well as the pubkey in `user-data.yml` in section users -> name: something -> ssh_authorized_keys.
- Modify `user-data.yml` according to your needs. 
- Deploy the VM onto Hetzner Cloud with `terraform init`, `terraform plan`, `terraform apply`. The usage of terraform commands isn't covered by this README.
