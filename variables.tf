variable hcloud_token {
    type = string
}

variable location {
    type = string
}

variable prefix {
    type = string
}

variable os_type {
    type = string
}

variable disk_size {
    type = string
}

variable server_type {
    type = string
}

variable ip_range {
    type = string
}

variable subnet_ip_range {
    type = string
}
